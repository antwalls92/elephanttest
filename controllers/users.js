
 class UserController{
    constructor(userModel){
        this.User = userModel;
    }

     deleteUser(req, res) {
        console.log('DELETE /user/');
        console.log(req.params);
    
            this.User.findById(req.params.id)
            .then((user,err) => {

                user.remove()
                .then((user,err) => {
                    if(err) 
                        return res.status(500).send(err.message);
                    res.status(200).send(user);
                })

            
        });
    };
    
    
     findById(req, res) {
        console.log('GET /user/' + req.params._id);
        console.log(req.params);
    
            this.User.findById(req.params.id, function(err, user) {
                console.log(JSON.stringify(user))
                if(err) 
                    return res.send(500, err.message);
                res.status(200).jsonp(user);
        });
    }
    
     updateUser(req, res) {
        console.log('PUT /user/');
        console.log(req.body);
    
        this.User.findById(req.params.id)
        .then((user,err) =>  {
            user.firstName = req.body.firstName;
            user.lastName  = req.body.lastName;
            user.email = req.body.email;
            user.role = req.body.role
    
            user.save()
            .then((user,err) => {
                console.log(JSON.stringify(user))
                if(err) 
                    return res.status(500).send(err.message);
                res.status(200).jsonp(user);
            });
        });
    };
    
     addUser(req, res) {
        console.log('POST user');
        console.log(req.body);
    
        var user = new this.User({
            firstName:  req.body.firstName,
            lastName:   req.body.lastName,
            email:  req.body.email,
            role:  req.body.role
        });
    
        user.save() 
        .then((user,err) =>  {
            console.log(JSON.stringify(user))
            if(err) 
                return res.status(500).send( err.message);
            res.status(200).jsonp(user);
        });
    }
    
     findAllUsers(req, res) {
        console.log('GET /users')
        console.log(req.body);
    
        this.User.find()
        .then((user,err) =>  {
            console.log(JSON.stringify(user))
            if(err) 
                res.send(500, err.message);
            res.status(200).jsonp(user);
        })
    }
}

module.exports = UserController