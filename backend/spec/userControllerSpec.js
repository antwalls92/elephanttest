require('jasmine')

var UserController = require("../controllers/users")

describe("When a new user is created", function(){
    let userController;
    
    beforeEach(function(){
        var userModel = jasmine.createSpyObj('UserModel', ['findById']);
        var users = [{"_id":"5f2187bccbdce62278bb106b","firstName":"Antonio","lastName":"Paredes","email":"frikitonio@hotmail.com","__v":0,"role":"Artist"},{"_id":"5f21c1cccbdce62278bb1075","firstName":"Pepaaaaaa","lastName":"Pig","email":"pepa@hotmail.com","__v":0,"role":"Designer"},{"_id":"5f21c38ecbdce62278bb1077","firstName":"Antonio","lastName":"Paredes","email":"frikitonio@hotmail.com","__v":0,"role":"ArtManager"},{"_id":"5f21caa88f3f2437dca9b902","firstName":"Antonio","lastName":"Paredes","email":"frikitonio@hotmail.com","role":"Artist","__v":0},{"_id":"5f21e87c8f3f2437dca9b903","firstName":"Antonio","lastName":"Paredes","email":"frikitonio@hotmail.com","role":"Designer","__v":0},{"_id":"5f23098cb5efa53704276001","firstName":"John","lastName":"Petrucci","email":"johnpetrucci@gmail.com","role":"Artist","__v":0},{"_id":"5f230a07b5efa53704276002","firstName":"Antonio","lastName":"Paredes","email":"frikitonio@hotmail.com","role":"ArtManager","__v":0},{"_id":"5f232b6079412a0630c4f34a","firstName":"bbbbbbbbbb","lastName":"xxxxxxxxxxxxx","email":"frikitonio@hotmail.com","role":"Artist","__v":0}]
        spyOn(userModel, "findById").and.returnValue(users);
        userController = new UserController(userModel);
    })
    
    it("it email already exists, return an error", function(){

        userController.addUser(req, res).bind(userController);
    });
});