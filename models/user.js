var mongoose = require('mongoose');
Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    firstName: {type: String, required: true},
    lastName: {type: String,  required: true},
    email: {type: String,  required: true, unique: true},
    role: {type: String,  required: true, enum: ['Artist','Designer','ArtManager']}
})


ValidateUniqueArtManager = function(next){
    var self = this;
    if(self.role === 'ArtManager'){
        console.log("is ArtManager");
        userModel.find({role : 'ArtManager', email: {$ne : self.email}}, function (err, docs) {
            console.log("err "+ JSON.stringify(err));
            console.log("docs "+ JSON.stringify(docs));
            if (!docs.length){
                next();
            }else{           
                console.log('User Art Manager exists: ',self.email);
                next(new Error("There is one ArtManager already"));
            }
        });
    }
    next();
     
}
userSchema.pre('save', ValidateUniqueArtManager)

var userModel = mongoose.model('User', userSchema);

module.exports = userModel;