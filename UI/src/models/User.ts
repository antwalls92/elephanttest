import { Role } from './Role';


export class User {
  constructor(
    public _id: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public role: Role) {

  }
}


