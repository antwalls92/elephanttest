import { Component, OnInit, Input, Inject } from '@angular/core';
import { User } from 'src/models/User';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { Role } from 'src/models/Role';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {

  user: User;

  form: FormGroup;
  description: string = "User";

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<UserModalComponent>, @Inject(MAT_DIALOG_DATA) data) {
      this.user = data.user;
  }

  onSubmit() {
    this.dialogRef.close(this.form.value as User);
  }

//   emailValidator = Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
  roleValidator(control: AbstractControl) : { [key: string]: boolean } | null {
    var roles = Object.values(Role)
    if (control.value &&   Object.values(Role).includes(control.value)) {
        return null;
      }
    return {'roleCheck' : false};
  }

  ngOnInit(): void {
      this.form = this.fb.group({
          '_id' : ['', []],
          'email' : ['', [Validators.required, Validators.email]],
          'firstName' : ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z]+$')]],
          'lastName' : ['', [Validators.required, Validators.maxLength(50) ,Validators.pattern('^[a-zA-Z]+$')]],
          'role' : ['', [Validators.required, this.roleValidator]]
        }
      );
      this.form.patchValue({...this.user});
  }

    close() {
        this.dialogRef.close();
    }

}


