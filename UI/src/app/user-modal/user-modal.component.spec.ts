import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { UserModalComponent } from './user-modal.component';
import { User } from 'src/models/User';
import { Role } from 'src/models/Role';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('UserModalComponent', () => {
  let component: UserModalComponent;
  let fixture: ComponentFixture<UserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserModalComponent ],
      imports:[ FormsModule, ReactiveFormsModule, MatDialogModule],
      providers:[ MatDialogRef]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserModalComponent);
    component = fixture.componentInstance;
    component.user = new User('dff6fu8yg8yg', 'Frank', 'Gambale', 'frankgambale@gmail.com', Role.Artist);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill the fields when a User is provided', () => {
    const form = fixture.debugElement.nativeElement.querySelector('form');

    const id = form.querySelector('input[formcontrolname="id"]').value
    const firstName = form.querySelector('input[formcontrolname="firstName"]').value
    const lastName = form.querySelector('input[formcontrolname="lastName"]').value
    const email = form.querySelector('input[formcontrolname="email"]').value
    const role = form.querySelector('input[formcontrolname="role"]').value

    expect(id).toBe('dff6fu8yg8yg')
    expect(firstName).toBe('Frank');
    expect(lastName).toBe('Gambale');
    expect(email).toBe('frankgambale@gmail.com');
    expect(role).toBe('Role.Artist');
  });

  it('should validate the input correctly', () => {
    throw new Error("Not implemented yet")
  });

  it('should return the User im the form', () => {
    throw new Error("Not implemented yet")
  });

  it('should select Artist by default', () => {
    throw new Error("Not implemented yet")
  });
  
});
