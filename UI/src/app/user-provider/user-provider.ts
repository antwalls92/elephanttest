
import { Injectable } from '@angular/core';
import { User } from 'src/models/User';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root',
})
export class UserProvider{

  url: string = environment.apiUrl;

  constructor(private http: HttpClient) {}

  GetUsers(): Observable<Array<User>> {
    return this.http.get(this.url) as Observable<Array<User>>;
  }
  DeleteUser(user: User): Observable<any> {
     const id = user._id;
     const url = this.url + "/" + id;
     return this.http.post(url, {});
  }
  CreateUser(user: User): Observable<User> {
    return this.http.post<User>(this.url, user) as Observable<User> ;
  }
  UpdateUser(user: User): Observable<User> {
    return this.http.put<User>(this.url+ "/" + user._id, user) as Observable<User>;
  }

}
