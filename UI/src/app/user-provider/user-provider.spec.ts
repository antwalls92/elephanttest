import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { UserProvider } from './user-provider';
import { User } from 'src/models/User';
import { Role } from 'src/models/Role';

describe('UserProviderComponent', () => {
  let service: UserProvider;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ UserProvider ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(UserProvider);
  }));


  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should return users when the service is ok', () => {

    const mockUsers = [new User('','','','', Role.Designer)];

    service.GetUsers().subscribe( users => {
        expect(users.length).toBe(1);
    });

    const req = httpTestingController.expectOne(service.url);
    expect(req.request.method).toEqual('GET');
    req.flush(mockUsers);

  });

  it('should store users when the service is ok', () => {
    const mockUser = new User('','','','', Role.Designer);

    service.CreateUser(mockUser).subscribe( user => {
        expect(user.role).toBe(Role.Designer);
    });

    const req = httpTestingController.expectOne(service.url);
    expect(req.request.method).toEqual('POST');
    req.flush(mockUser);
  });

  it('should delete users when the service is ok', () => {
    const mockUser = new User('','','Paredes','', Role.Designer);

    service.DeleteUser(mockUser).subscribe( user => {
        expect(user.lastName).toBe('Paredes');
    });

    const req = httpTestingController.expectOne(service.url+ '/' + mockUser._id);
    expect(req.request.method).toEqual('POST');
    req.flush(mockUser);
  });

  it('should update users when the service is ok', () => {
    const mockUser = new User('','Antonio','','', Role.Designer);

    service.UpdateUser(mockUser).subscribe( user => {
        expect(user.firstName).toBe('Antonio');
    });

    const req = httpTestingController.expectOne(service.url + '/' + mockUser._id);
    expect(req.request.method).toEqual('PUT');
    req.flush(mockUser);
  });

  it('should return a 500 if you request users when the service is Ko', () => {
    throw new Error("Not implemented yet")
  });

  it('should return a 500 if you request to store users when the service is Ko', () => {
    throw new Error("Not implemented yet")
  });

  it('should return a 500 if you request to delete users when the service is Ko', () => {
    throw new Error("Not implemented yet")
  });

  it('should return a 500 if you request to update users when the service is Ko', () => {
    throw new Error("Not implemented yet")
  });

});
