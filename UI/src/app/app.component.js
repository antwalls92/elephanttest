"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppComponent = void 0;
var core_1 = require("@angular/core");
var User_1 = require("./User");
var table_1 = require("@angular/material/table");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'elephant';
        this.columns = ['FirstName', 'LastName', 'Email', 'Operations'];
        this.data = [new User_1.User('Antonio', 'Paredes', 'antwalls92@gmail.com')];
        this.selecteditem = this.data[0];
    }
    AppComponent.prototype.deleteRow = function (index) {
        if (confirm('Are you sure you want to delete it?')) {
            this.data.splice(index, 1);
            this.userTable.renderRows();
        }
    };
    AppComponent.prototype.editRow = function (index) {
        var user = this.readUserForm();
        if (index > 0) {
            this.data[index] = user;
            this.userTable.renderRows();
        }
    };
    AppComponent.prototype.addRow = function () {
        var user = this.readUserForm();
        this.data.push(user);
        this.userTable.renderRows();
        this.selecteditem = new User_1.User('', '', '');
    };
    AppComponent.prototype.readUserForm = function () {
        return new User_1.User(this.selecteditem.firstName, this.selecteditem.lastName, this.selecteditem.email);
    };
    __decorate([
        core_1.ViewChild(table_1.MatTable)
    ], AppComponent.prototype, "userTable", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
