import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'elephant'`, () => {
    const app = fixture.componentInstance;
    expect(app.title).toEqual('elephant');
  });


  it('should have a component user-list ', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-user-list')).not.toBe(null);
  });

});
