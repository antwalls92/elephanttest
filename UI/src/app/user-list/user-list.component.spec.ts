import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListComponent } from './user-list.component';
import { UserFilterComponent } from '../user-filter/user-filter.component';
import { UserProvider } from '../user-provider/user-provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { User } from 'src/models/User';
import { Role } from 'src/models/Role';
import { MatTable, MatTableModule } from '@angular/material/table';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent, UserFilterComponent ],
      providers:[UserProvider],
      imports: [HttpClientTestingModule, MatDialogModule, MatTableModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    component.data = [
      new User('aff6fu8yg8yg', 'Antonio', 'Paredes', 'antwalls92@gmail.com', Role.ArtManager),
      new User('bff6fu8yg8yg', 'Pepa', 'pig', 'pepapig@gmail.com', Role.Artist),
      new User('cff6fu8yg8yg', 'John', 'Petrucci', 'aaaaa@gmail.com', Role.Designer),
      new User('dff6fu8yg8yg', 'Frank', 'Gambale', 'frankgambale@gmail.com', Role.Artist),
    ]

    // const filter = fixture.debugElement.query(By.css('app-user-filter'));
    // filter.nativeElement.dispatchEvent(new Event('filterChange'));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the filter present', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-user-filter')).not.toBe(null);
  });

  it('should show the expected elements', () => {
    const rows = fixture.debugElement.queryAll(By.css('table tr'));
    component.onFilterChange(component.data);
    expect(rows.length).toBe(4);
  });

  it('should open the modal for creating a user when the create button is hit', () => {
    component.createRow();

    const form = fixture.debugElement.nativeElement.querySelector('form');

    const id = form.querySelector('input[formcontrolname="id"]')
    const firstName = form.querySelector('input[formcontrolname="firstName"]')
    const lastName = form.querySelector('input[formcontrolname="lastName"]')
    const email = form.querySelector('input[formcontrolname="email"]')
    const role = form.querySelector('input[formcontrolname="role"]')

    expect(id).toBe('')
    expect(firstName).toBe('');
    expect(lastName).toBe('');
    expect(email).toBe('');
    expect(role).toBe('');

  });

  it('should open the modal for edit a user when the edit button is hit at one item', () => {
    throw new Error("Not implemented yet")
  });

  it('should create the user when you call create', () => {
    throw new Error("Not implemented yet")
  });

  it('should edit the user when you call edit', () => {
    throw new Error("Not implemented yet")
  });

  it('should delete the user when you call delete', () => {
    throw new Error("Not implemented yet")
  });



});
