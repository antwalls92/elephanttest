import { Component, OnInit, Self, ViewChild } from '@angular/core';
import { User } from '../../models/User';
import { MatTable } from '@angular/material/table';
import { UserProvider } from '../user-provider/user-provider';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { Observable } from 'rxjs';
import { Role } from 'src/models/Role';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers:[UserProvider]
})
export class UserListComponent implements OnInit {

  title = 'elephant';
  columns: string[] = ['FirstName', 'LastName', 'Email', 'Role', 'Operations'];
  data: User[] = [];
  filteredData: User[] = [];
  selecteditem: User = new User( '', '', '',  '' , Role.Artist);

  @ViewChild(MatTable) userTable: MatTable<User>;

  constructor(public userProvider: UserProvider, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.updateData();
  }

  updateData(): void{
    this.userProvider.GetUsers().subscribe( (data) => {
      this.data = data;
      this.filteredData = data;
      this.userTable.renderRows();
     },
     (error) => {
       console.log(error);
     });
  }

  createRow(): void{
    this.showEditModal(new User('','','','', Role.Artist))
    .subscribe(result => {
      if(!result) { return; }
      this.userProvider.CreateUser(result).subscribe(data =>{
        this.updateData();
      },
      error => {
        console.log("KO " + JSON.stringify(error))
      }) ; 
    });
  }


  deleteRow(index: number): void{
    if(confirm('Are you sure you want to delete it?')){
      const user = this.filteredData[index];
      this.userProvider.DeleteUser(user).subscribe(data =>{
        this.updateData();
      },
      error => {
        console.log("KO " + JSON.stringify(error))
      }) ;
    }
  }

  editRow(index: number): void{
    const user = {... this.filteredData[index]};
    this.showEditModal(user)
    .subscribe(result => {
      if(!result) { return; }
      this.userProvider.UpdateUser(result as User).subscribe(data =>{
        this.updateData();
      },
      error => {
        console.log("KO " + JSON.stringify(error))
      });
    });
  }


  showEditModal(user: User): Observable<User>{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { user }; 

    const dialogRef = this.dialog.open(UserModalComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  onFilterChange(event: any): void{
    this.filteredData = event;
    this.userTable.renderRows();
  }

}

