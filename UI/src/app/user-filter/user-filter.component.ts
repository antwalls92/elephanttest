import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/models/User';
import { Role } from 'src/models/Role';

@Component({
  selector: 'app-user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.css']
})
export class UserFilterComponent implements OnInit {

  search: string = '';
  role: Role = undefined;

  @Input()
  users: Array<User>;
  filteredUsers: Array<User>;

  @Output()
  filterChange: EventEmitter<Array<User>>;

  constructor() {
    this.filterChange = new EventEmitter<Array<User>>();
  }

  ngOnInit(): void {}

  filter(event: any): void{
    this.filteredUsers = this.users.filter( x =>
      this.role === undefined || x.role === this.role)
      .filter( x =>
        x.firstName?.includes(this.search) ||
        x.lastName?.includes(this.search) ||
        x.email?.includes(this.search))

    this.filterChange.emit(this.filteredUsers);
  }
  
}
