import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFilterComponent } from './user-filter.component';
import { By } from '@angular/platform-browser';
import { User } from 'src/models/User';
import { Role } from 'src/models/Role';

describe('UserFilterComponent', () => {
  let component: UserFilterComponent;
  let fixture: ComponentFixture<UserFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFilterComponent);
    component = fixture.componentInstance;
    component.users = [
      new User('aff6fu8yg8yg', 'Antonio', 'Paredes', 'antwalls92@gmail.com', Role.ArtManager),
      new User('bff6fu8yg8yg', 'Pepa', 'pig', 'pepapig@gmail.com', Role.Artist),
      new User('cff6fu8yg8yg', 'John', 'Petrucci', 'aaaaa@gmail.com', Role.Designer),
      new User('dff6fu8yg8yg', 'Frank', 'Gambale', 'frankgambale@gmail.com', Role.Artist),
    ]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a search box to filter', () => {
    const search = fixture.debugElement.query(By.css('input[name="search"]'));
    expect(search).not.toBe(null);
  });
  it('should have a select to filter by role', () => {
    const select = fixture.debugElement.query(By.css('mat-select[name="role"]'));
    expect(select).not.toBe(null);
  });

  it('should filter when you type on the search box', () => {
    const search = fixture.debugElement.query(By.css('input[name="search"]'));

    component.search = "aaaaa";
    search.nativeElement.dispatchEvent(new Event('keydown'));

    expect(component.filteredUsers.length).toBe(1);

  });
  it('should filter when you change the value on the select', () => {
    const select = fixture.debugElement.query(By.css('mat-select[name="role"]'));

    component.role = Role.Artist;
    select.nativeElement.dispatchEvent(new Event('selectionChange'));

    expect(component.filteredUsers.length).toBe(2);
  });
  it('should not filter when the value ALL is the selected', () => {
    const select = fixture.debugElement.query(By.css('mat-select[name="role"]'));

    component.role = Role.Artist;

    fixture.detectChanges();
    select.nativeElement.dispatchEvent(new Event('selectionChange'));

    component.role = undefined;

    fixture.detectChanges();
    select.nativeElement.dispatchEvent(new Event('selectionChange'));

    expect(component.filteredUsers.length).toBe(4);
  });
  it('should not filter when the searchbox is emptied', () => {
    const search = fixture.debugElement.query(By.css('input[name="search"]'));

    component.search = "aaaaa";
    search.nativeElement.dispatchEvent(new Event('keydown'));

    expect(component.filteredUsers.length).toBe(1);

    component.search = "";
    search.nativeElement.dispatchEvent(new Event('keydown'));

    expect(component.filteredUsers.length).toBe(4);
  });

});
