import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { UserListComponent } from './user-list/user-list.component';
import { UserProvider } from './user-provider/user-provider';
import { UserModalComponent } from './user-modal/user-modal.component';
import { MatSelectModule } from '@angular/material/select';
import { UserFilterComponent } from './user-filter/user-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserModalComponent,
    UserFilterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    HttpClientModule
  ],
  providers: [UserProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
