var express = require("express");
var mongoose = require("mongoose");
var bodyParser  = require("body-parser");
const path = require('path');
const fs = require('fs');
var mongoose = require('mongoose');
var methodOverride = require("method-override");

//model
require("./models/user")
var User  = mongoose.model('User');

//controllers
var UserController = require("./controllers/users")

//Express server
var app = express();

var config = {};

var args = process.argv.slice(2);
console.log('args: ', args);
env = args[0] || "develop" 

if(env == "develop" ){
  let rawdata = fs.readFileSync('config.develop.json');
  config = JSON.parse(rawdata);
}else{
  let rawdata = fs.readFileSync('config.production.json');
  config = JSON.parse(rawdata);
}

//midlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());


var uiStaticPath = __dirname + '/UI/dist/elephant';
app.use(express.static(uiStaticPath));

app.use(bodyParser.urlencoded({
    extended: true
  }));

//UI
app.route('/')
.get(function(req, res){
    res.sendFile(path.join(__dirname, '/UI/dist/elephant/index.html'));
})


var userController = new UserController(User)
//Api
app.route('/api/users/:id')
.get(userController.findById.bind(userController))
.put(userController.updateUser.bind(userController))
.post(userController.deleteUser.bind(userController))

app.route('/api/users')
.get(userController.findAllUsers.bind(userController))
.post(userController.addUser.bind(userController))

var mongoConectionString = config.databaseConnectionString;

mongoose.connect(mongoConectionString, function(err, res){
  if(err) {
          console.log('ERROR: connecting to Database. ' + mongoConectionString + ' ' + err);
      }
      app.listen(process.env.PORT || 3000, function() {
      console.log("Node server running on http://localhost:3000");
      });
})
